export default {
	currentIndex:0,
	lastIndex: selector_previewer.options.length -1,
	viewer(){
		const currentData = appsmith.store?.resourcesDAta?.filter(resource => resource._id === appsmith.URL.queryParams._id).map(({assets, guideUrl}) => ({assets,guideUrl}))
		console.log(currentData, 'resorudeDAta?')
		storeValue('assetsPreview',...currentData,true)
	},
	assetsList(){
		const list = appsmith.store?.resourcesDAta?.map((asset)=> asset)
		storeValue('assetsSelector', list, false)
	},
	backArrowSelection() {
		if (this.currentIndex > 0) {
			this.currentIndex--; 
			selector_previewer.setSelectedOption(selector_previewer.options[this.currentIndex].value);
		}
		this.updateButtonStates();
	},
	fowardArrow() {
		if (this.currentIndex < this.lastIndex) {
			this.currentIndex++;
			selector_previewer.setSelectedOption(selector_previewer.options[this.currentIndex].value);
		}
		this.updateButtonStates();
	},
	updateButtonStates() {
		back_previewer.setDisabled(this.currentIndex === 0); 
		forward_previewer.setDisabled(this.currentIndex === this.lastIndex); 
	},
	onSelectorOptionChange() {
		this.currentIndex = selector_previewer.options.findIndex(option => option.value === selector_previewer.selectedOptionValue);
		this.updateButtonStates();
	},
	urlFrame(){
		const selectedValue = selector_previewer.selectedOptionValue;
		const selectedAsset = appsmith.store.assetsPreview.assets.find(asset => asset._id === selectedValue);
		return selectedAsset ? selectedAsset.url : null;
	}
}