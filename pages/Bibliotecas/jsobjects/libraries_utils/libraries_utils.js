export default {
	selectedOption: appsmith.URL.hostname.includes('develop') ? 'integration' : 'colegios',
	libraries: [],
	selectedLibraryID: undefined,
	loadDataErrorCount: 0,
	
	async getLibraries () {
		await listLibraries.run();

			const libraries = listLibraries.data.map(async (library) => {
				// if(library.tags.length === 0){
					return {...library, tags: library.tags.length}
				// }
				// console.log(library.tags.toString())
				// const tags = await findTags.run({ids: library.tags.toString()})
				// return {...library, tags: tags.map(tag => tag.name).toString()}
			});

			this.libraries = await Promise.all(libraries);
	},
	
	async loadData(){
		try {
			await this.getLibraries()
		} catch(err){
			Sentry.captureException(err)
			console.error(err)
			if(this.loadDataErrorCount < 3){
				await this.getLibraries()
				
				this.loadDataErrorCount += 1
			}
		}
	}
}