export default {
	pollingIntervalId: null,
	hasUrl:replace_url_google.text.trim() !== '',
	hasFile:replace_upload_asset.files.length > 0,

	resetWidgets(){
		resetWidget("replace_asset_title")
		resetWidget("replace_subject_content_select")
		resetWidget("replace_upload_asset")
		resetWidget("replace_url_google")
	},
	disableReplaceAssetBtn(){
		return (!this.hasUrl && !this.hasFile)
	},
	saveReplaceData(){
		const assetPosition = {
			assetId: assets_list_table.triggeredRow._id,
			assetPosition: assets_list_table.triggeredRowIndex
		}
		storeValue('position', assetPosition,false)
		showModal(replace_asset_modal.name)
	},

	currentAssetList(newAsset){
		const currentAssetList = appsmith.store.assetList || []
		const {assetPosition} = appsmith.store.position

		const updatedAssetList = [...currentAssetList]
		updatedAssetList[assetPosition] = newAsset
		storeValue("assetList", updatedAssetList, false);
	},
	async replaceAssetOnList(){
		closeModal(replace_asset_modal.name)
		if(this.hasUrl){
			await this.uploadAssetByUrl()
		}
		if(this.hasFile){
			await this.uploadAssetByFile()
		}
	},

	async uploadAssetByFile(){
		const tempassetId = appsmith.store.position
		
		console.log('subiendo asset para su reemplazo', tempassetId.assetId )
		
		const assetVersionId = (new bson.ObjectId()).toString()
		const assetToUpload = replace_upload_asset.files?.find(file => file)
		const file_path = `assets/${tempassetId.assetId}/versions/${assetVersionId}/${assetToUpload?.name}`
		
		const newAsset = {
			_id: tempassetId.assetId,
			assetVersionId,
			unprocessedUrl: file_path,
			title: assets_list_table.triggeredRow.title,
			status: "in-progress",
			contentType: replace_subject_content_select.selectedOptionValue
		}
		this.currentAssetList(newAsset)
		this.resetWidgets()
	

		update_button.setDisabled(true)
		await this.uploadAssets(newAsset,assetToUpload,file_path)


	},
	async uploadAssets(newAsset,assetToUpload,file_path){
		// console.log(newAsset,assetToUpload,)
		try {
			await upload_asset_file.run({
				bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
				file_path,
				content: assetToUpload
			})

			if(upload_asset_file.data){
				console.log("archivo subido")
				await create_asset.run({body:newAsset})

				if(create_asset.data){
					await showAlert(`Asset ${newAsset.title} creado con exito!`, "success").then(()=> update_button.setDisabled(false))
				}
			}
		} catch(err){
			console.error(err)
			update_button.setDisabled(false)
			await showAlert(`Ocurrio un error creando el Asset: ${err}`, 'error')
		}
	},

	async uploadAssetByUrl(){
		try{
			storeValue('googleUrl', replace_url_google.text, false)
			const response = await upload_from_googleDrive.run()
			console.log(response,'google response')
			if(response){
				resetWidget('url_input')

				const newAsset ={
					_id: response._id,
					title: response.title,
					status: response.status
				}
				this.currentAssetList(newAsset)
				showAlert('Asset agregado desde URL de google','success')

			}else{
				const errorMessage = response.data.error
				throw new Error(errorMessage)
			}
		}catch(err){
			console.error(err)
			showAlert(`${err.message}`, 'error')
		}
	},
}