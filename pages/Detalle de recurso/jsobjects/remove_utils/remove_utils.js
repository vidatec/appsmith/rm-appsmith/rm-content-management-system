export default {
	async removeAssetFromList (id) {
		const newList = appsmith.store.assetList.filter(item => item._id !== id)
		storeValue('assetList', newList)
	}
}