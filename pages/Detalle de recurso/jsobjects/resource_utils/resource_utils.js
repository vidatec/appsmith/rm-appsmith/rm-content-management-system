export default {
	typeTagsToSearch: [
		'materia','nivel','contenido','reading_fluency_level',
		'pilar_pedagogico','temas_generales','situacion de ensenanza',
		'module','pais','modalidad','capacidades','eje','género','submodulo_robotica',
		'language','microcontenidos','submodulo_comprension_lectora',
		'alfabetizacion','actividad','ma_tipo_cursada','ma_espacio_curricular','ma_bimestre','ma_area','ma_codigo_area','ma_codigo_espacio','ma_nivel_idiomas'
	],
	tagSelectors: [
		module_tags,
		modality_tags,
		pedagogical_tag,
		submodule_tags,
		activity_tags,
		gender_tags,
		microContent_tags,
		robotic_tags,
		literacy_tags,
		readingFluency_tags,
		language_tags,
		ejeTematico_tags,
		generalTopics_tags,
		Tipo_de_cursada,
		ma_espacio_curricular,
		ma_bimestre,
		ma_area,
		ma_codigo_area,
		ma_codigo_espacio,
		ma_nivel_idiomas
	],
	jurisdictions: [],
	subjects:[],
	levels:[],
	tagsResults: {},
	pollingIntervalId: null,
	currentResource: {},
	existResource: !!appsmith.URL.queryParams._id || !!appsmith.URL.queryParams.environment || resource_detail.isLoading,
	hasUrl:url_google_input.text.trim() !== '',
	hasFile:upload_file_asset.files.length > 0,
	environment: appsmith.URL.queryParams.environment,
	selectedEnvironmentOption: appsmith.URL.hostname.includes('develop') ? 'integration' : appsmith.URL.queryParams.environment,

	allAssets:[],

	async loadAllAssets(){
		await get_assets_list.run()
		this.allAssets = await get_assets_list.data
		return this.allAssets
	},

	assetsListMemory(){
		const assetsListOnMemory = appsmith.store.assets_list_memory
		const newAssetsListOnMemory =[
			...appsmith.store.assetList,
			...assetsListOnMemory
		]
		console.log(newAssetsListOnMemory,'que hay en memoria')
		storeValue('assetList', newAssetsListOnMemory, false)
		closeModal(assets_list_modal.name)
		// assets_list_modal_table.setSelectedRowIndices([])
	},
	rowBeingEdited(){
		const rows = assets_list_table['editableCell'].index
		return rows !== -1
	},
	disableSaveButton(){
		try{
			const selectors = this.getSelectors?.data
			console.log()
			const hasAllData = 
						selectors?.jurisdictions?.length > 0 &&
						selectors?.levels?.length > 0 &&
						selectors?.subjects?.length > 0 &&
						selectors?.jurisdictionGroup?.length > 0;

			// Habilitar o deshabilitar el botón de guardar
			update_button.setDisabled(!hasAllData);
			console.log(!hasAllData)
			showAlert('Selectores cargados', 'success')
		}catch(error){
			showAlert('Ocurrio un problema cargando los selectores, podrias recargar la pagina', 'error', 6000)
		}

	},
	disableAssetToListBtn(){
		if(tabs.selectedTab === 'Subir'){
			return (!this.hasUrl && !this.hasFile)
		}
	},
	resetWidgets(){
		resetWidget("upload_file_asset")
		resetWidget("upload_title")
		resetWidget("subject_content_selector")
	},

	async initializeAssetTable(){
		const r = await resource_detail.run()
		this.currentResource = r
		const assets = r?.assets || [];
		storeValue('assetList', assets)
	},

	updateFrom(){
		const updatedFields = edit_resource_form.formData;
		const validFields = Object.keys(updatedFields).filter(key => key in this.currentResource);
		validFields.forEach(key => {
			this.currentResource[key] = updatedFields[key];
		});
	},

	updateLevelForm(){
		this.currentResource = {
			...this.currentResource,
			jurisdictions: jurisdiction_selector.selectedOptionValues,
			subjects: subject_selector.selectedOptionValues,
			levels: level_selector.selectedOptionValues,
		}
	},

	updateAttributeTagsForm(){
		const allTags = []
		this.tagSelectors.forEach(selector =>{
			if(selector.selectedOptionValues){
				allTags.push(...selector.selectedOptionValues)
			}
		})
		this.currentResource = {
			...this.currentResource,
			tags: allTags
		}
	},

	async loadTags() {
		const results = await Promise.all(
			this.typeTagsToSearch.map(type => tags.run({ type })
																.then(result => ({ 
				type, 
				data: result.map(tag => ({ label: tag.name, value: tag._id })) 
			}))
															 ));

		this.tagsResults = results.reduce((acc, { type, data }) => {
			acc[type] = data;
			return acc;
		}, {});

		storeValue('tagsResults', this.tagsResults);
	},

	async getSelectorsData(environment){
		let jurisdictionsData, levelsData, subjectsData, jurisdictionGroup;

		switch (environment) {
			case 'beta':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_beta.run(),
					get_levels_beta.run(),
					get_subjects_beta.run(),
					get_juris_groups_beta.run()
				]);

			case 'colegios':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_colegios.run(),
					get_levels_colegios.run(),
					get_subjects_colegios.run(),
					get_juris_groups_colegios.run()
				]);

			case 'profesional':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_profesional.run(),
					get_levels_profesional.run(),
					get_subjects_profesional.run(),
					get_juris_groups_profesional.run()
				]);

			case 'integration':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_integration.run(),
					get_levels_integration.run(),
					get_subjects_integration.run(),
					get_juris_groups_integration.run()
				]);

			default:
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_colegios.run(),
					get_levels_colegios.run(),
					get_subjects_colegios.run(),
					get_juris_groups_colegios.run()
				]);
		}
	},

	async getSelectors(){
		const [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await this.getSelectorsData(this.environment);
		return{
			jurisdictions: jurisdictionsData?.map(j =>({
				label: j.name,
				value: j.id
			})),
			levels: levelsData?.map(lv => ({
				label: lv.name,
				value: lv.id
			})),
			subjects: subjectsData?.map(s => ({
				label: s.name,
				value: s.id
			})),
			jurisdictionGroup: jurisdictionGroup
		}	
	},
	//pdf file validation
	pdfValidation(){
		const guideToUpload = guia_docente_upload.files?.find(file => file)
		if (!guideToUpload || !guideToUpload.type.startsWith("application/pdf")) {
			showAlert("Debes seleccionar un archivo que sea PDF.", "error")
			resetWidget('guia_docente_upload')
			return false
		}
		return true
	},
	async uploadGuiaDocente(){
		try{
			if(guia_docente_upload.files.length >= 1 && this.pdfValidation()){
				const resourceId = resource_detail.data._id
				const guideToUpload = guia_docente_upload.files?.find(file => file)
				const file_path = `resources/${resourceId}/${guideToUpload?.name}`

				console.log(resourceId, guideToUpload,file_path)

				const data = await upload_guia_docente.run({
					bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
					file_path,
					content: guideToUpload
				})

				if(data){
					showAlert('Guia guardada correctamente!', 'success', 4000)
					this.currentResource = {
						...this.currentResource,
						guideUrl: file_path
					}
					await storeValue('currentResoruce', this.currentResource, false)
					resetWidget('guia_docente_upload')
				}	
			} else{
				delete this.currentResource.guideUrl
				await storeValue('currentResoruce', this.currentResource)
			}
		} catch(err){
			console.log(err)
			showAlert(`Error al guarar el cover de la imagen! ${err}`, 'error', 4000)
		}
	},

	async uploadCoverImage(){
		try {
			if(cover_image.files.length >= 1){
				const resourceId = resource_detail.data._id
				const coverToUpload = cover_image.files?.find(file => file)
				const file_path = `resources/${resourceId}/${coverToUpload?.name}`

				console.log('se activa el uploadCoverImage')
				const data = await upload_cover_image.run({
					bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
					file_path,
					content: coverToUpload
				})
				if(data){
					showAlert('Cover guardado correctamente!', 'success', 4000)
					this.currentResource = {
						...this.currentResource,
						coverUrl: file_path
					}
					await storeValue('currentResoruce', this.currentResource, false)
					resetWidget('cover_image')
				}
			} else {
				delete this.currentResource.coverUrl
				await storeValue('currentResoruce', this.currentResource)
			}
		} catch(err){
			console.error(err)
			showAlert(`Error al guarar el cover de la imagen! ${err}`, 'error', 4000)
		}
	},

	async uploadAssetByUrl(){
		try{
			storeValue('googleUrl', url_google_input.text.trim(), false)
			const response = await upload_from_googleDrive.run()
			console.log(response,'google response')
			if(response){
				resetWidget('url_google_input')

				const newAsset ={
					_id: response._id,
					title: response.title,
					status: response.status
				}
				const currentAssetList = appsmith.store.assetList || []
				const updatedAssetList = [...currentAssetList, newAsset]
				console.log(updatedAssetList, 'DEBERIA TENER EL ASSET ID AGREGADO DE LA URL DE GOOGLE')
				showAlert('Asset agregado desde URL de google','success')
				storeValue("assetList", updatedAssetList, false);

			}else{
				const errorMessage = response.data.error
				throw new Error(errorMessage)
			}
		}catch(err){
			console.error(err)
			showAlert(`${err.message}`, 'error')
		}
	},
	async uploadAssetByFile(){
		const assetId = (new bson.ObjectId()).toString()
		const assetVersionId = (new bson.ObjectId()).toString()
		const assetToUpload = upload_file_asset.files?.find(file => file)
		const file_path = `assets/${assetId}/versions/${assetVersionId}/${assetToUpload?.name}`

		const newAsset = {
			_id: assetId,
			assetVersionId,
			unprocessedUrl: file_path,
			title: upload_title.text,
			status: "in-progress",
			contentType: subject_content_selector.selectedOptionValue
		}
		const currentAssetList = appsmith.store.assetList || []
		const updatedAssetList = [...currentAssetList, newAsset]
		console.log(updatedAssetList, 'DEBERIA TENER EL ASSET AGREGADO')

		this.resetWidgets()

		if(tabs.selectedTab === 'Subir'){
			update_button.setDisabled(true)
			console.log('se activa el subir')
			await this.uploadAssets(newAsset,assetToUpload,file_path)
			storeValue("assetList", updatedAssetList, false);
		}

	},
	async addAssetsToList(){

		if(this.hasUrl){
			update_button.setDisabled(true)
			await this.uploadAssetByUrl()
			update_button.setDisabled(false)
		}
		if(this.hasFile){
			update_button.setDisabled(true)
			await this.uploadAssetByFile()
			update_button.setDisabled(false)
		}

	},

	async uploadAssets(newAsset,assetToUpload,file_path){
		// console.log(newAsset,assetToUpload,)
		try {
			await upload_asset_file.run({
				bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
				file_path,
				content: assetToUpload
			})

			if(upload_asset_file.data){
				console.log("archivo subido")
				await create_asset.run({body:newAsset})
				console.log(create_asset.data, 'data creada')
				if(create_asset.data){
					await showAlert(`Asset ${newAsset.title} creado con exito!`, "success").then(()=> update_button.setDisabled(false))
				}
			}
		} catch(err){
			console.error({err})
			update_button.setDisabled(false)
			await showAlert(`Ocurrio un error creando el Asset: ${err.message}`, 'error')
		}
	},

	// async pollingAssets(){
	// try{
	// const {assets} = await resource_detail.run()
	// // storeValue("assetList", assets, false)
	// const allProcessed = assets.every(asset => asset.status !== "in-progress")
	// 
	// if (allProcessed) {
	// clearInterval(this.pollingIntervalId);
	// console.log("Polling detenido: todos los assets procesados")
	// storeValue("assetList", assets);
	// update_button.setDisabled(false)
	// }else{
	// 
	// console.log("Polling detenido: todos los assets procesados")
	// storeValue("assetList", assets)
	// update_button.setDisabled(false)
	// }
	// 
	// }catch(err){
	// console.error(err)
	// showAlert(`Ocurrio un error: ${err}`, 'error')
	// }
	// },
	// async pollingInit(){
	// update_button.setDisabled(true)
	// // init the polling if there a "in-progress" asset
	// if (appsmith.store.assetList.some(asset => asset.status === "in-progress")) {
	// this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
	// console.log("Polling iniciado algun asset con in-progress");
	// }else{
	// console.log("Polling iniciado por url de google");
	// this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
	// }
	// },
	// async pollingAssets() {
	// try {
	// const { assets } = await resource_detail.run(); // Llama al recurso
	// const allProcessed = assets.every(asset => asset.status !== "in-progress");
	// 
	// if (allProcessed) {
	// clearInterval(this.pollingIntervalId); // Detén el intervalo
	// console.log("Polling detenido: todos los assets procesados");
	// }
	// 
	// // Actualiza los datos y el estado del botón
	// storeValue("assetList", assets); // Solo almacena los datos finales
	// update_button.setDisabled(!allProcessed); // Habilita el botón solo si todos están procesados
	// 
	// } catch (err) {
	// console.error(err);
	// showAlert(`Ocurrió un error: ${err}`, 'error');
	// clearInterval(this.pollingIntervalId); // Detén el intervalo en caso de error
	// }
	// },
	// async pollingInit() {
	// // Deshabilita el botón mientras inicia el polling
	// update_button.setDisabled(true);
	// 
	// // Verifica si hay algún asset con "in-progress" y configura el polling
	// const { assets } = await resource_detail.run();
	// const hasInProgress = assets.some(asset => asset.status === "in-progress");
	// 
	// if (hasInProgress) {
	// this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
	// console.log("Polling iniciado: hay assets con 'in-progress'");
	// } else {
	// // No inicia el polling si no hay assets en proceso
	// console.log("No hay assets con 'in-progress'.");
	// storeValue("assetList", assets); // Almacena los datos iniciales
	// update_button.setDisabled(false); // Habilita el botón
	// }
	// },

	async updateAsset(){
		update_button.setDisabled(true)
		try{
			await Promise.all([
				this.uploadCoverImage(),
				this.uploadGuiaDocente()
			]);

			const updatedResource = {
				...this.currentResource,
				assets: appsmith.store.assetList.map(({_id}) => _id),
			};

			console.log(this.currentResource,'que tiene el recurso antes de ser guardado??')
			for (const key in updatedResource) {
				if (Array.isArray(updatedResource[key]) && updatedResource[key].length === 0) {
					delete updatedResource[key];
				}
			}
			storeValue('updatedResource', updatedResource)
			console.log(updatedResource, 'antes de guardar todo')

			await update_resource.run().then((showAlert('Guardado', 'success'))).then(() => update_button.setDisabled(false)).catch(er => console.log(er))
			if(tabs.selectedTab === 'Subir'){
				this.pollingInit()
			}

		}catch(err){
			console.error(err)
			showAlert(`Ocurrio un error al guargar: ${err}`, 'error')
			update_button.setDisabled(true)
		}

	},
	async updateAssetOnListTable(){
		const {_id, title} = await get_asset_api.run()
		console.log(_id)
		const updatedAssetList = appsmith.store.assetList.map(asset => {
			if (asset._id === _id) {
				return { ...asset, title }; 
			} else {
				return asset;
			}
		});
		storeValue("assetList", updatedAssetList);
	},

	async updateAssetTilte(){

		const newTitle = assets_list_table.updatedRow.title
		const currentRowId = assets_list_table.updatedRow._id
		const currentTitle = appsmith.store.assetList.find(asset => asset._id === currentRowId)?.title
		storeValue('assetTitle', newTitle, false)
		if(newTitle !== currentTitle){
			update_button.setDisabled(true)
			console.log('TAMO ACAAAAAAAA!')
			await update_asset_api.run()
				.then(()=> this.updateAssetOnListTable())
				.then(()=> showAlert(`Asset actulizado`, 'success'))
			update_button.setDisabled(false)
		}
	},
	// polling de status de assets
	async pollingAssets() {
		try {
			const { assets } = await resource_detail.run(); // Fetch the assets
			const allProcessed = assets.every(asset => asset.status !== "in-progress");

			storeValue("assetList", assets); // Update the store with the latest assets

			if (allProcessed) {
				clearInterval(this.pollingIntervalId); // Stop the interval when all are processed
				console.log("Polling detenido: todos los assets procesados");
				storeValue("isPolling", false); // Update progress state
				update_button.setDisabled(false); // Enable the button
			}

		} catch (err) {
			console.error(err);
			clearInterval(this.pollingIntervalId); // Stop polling on error
			storeValue("isPolling", false); // Update progress state
			showAlert(`Ocurrió un error: ${err}`, 'error');
		}
	},
	async pollingInit(criteria = "default") {
		update_button.setDisabled(true); // Disable the button during polling
		storeValue("isPolling", true); // Indicate polling is active

		// Fetch assets and apply initiation criteria
		const { assets } = await resource_detail.run();
		const shouldPoll = criteria === "in-progress" 
		? assets.some(asset => asset.status === "in-progress") 
		: true; // Default behavior is to start polling

		if (shouldPoll) {
			this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
			console.log(`Polling iniciado con criterio: ${criteria}`);
		} else {
			console.log("No hay necesidad de iniciar el polling.");
			storeValue("isPolling", false); // No polling needed
			update_button.setDisabled(false); // Enable the button
		}
	},
	stopPolling() {
		clearInterval(this.pollingIntervalId); // Stop the interval
		storeValue("isPolling", false); // Update progress state
		update_button.setDisabled(false); // Enable the button
		console.log("Polling detenido manualmente");
	},
	// fin de polling de status de assets
}