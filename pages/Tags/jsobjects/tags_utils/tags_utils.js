export default {
	tags: [],
	tagsLoading: true,

	async getTags(){
		await get_all_tags.run()
		this.tags = get_all_tags.data.filter(tag => tag.type !== 'custom').map(tag => ({_id: tag._id, name: tag.name, type: tag.type}))
		this.tagsLoading = false
		return this.tags
	},

	async loadData(){
		await Promise.all([
			this.getTags()(),
		])
	},

	async saveTag(){
		const tagToSave = {name: tag_name.text, type: tag_type.selectedOptionValue}
		storeValue('newTag', tagToSave, false)
	},

	clearFields(){
		tag_name.setValue('')
	}
}