export default {
	workflows: [],
	
	async fixer ({assetId}) {
		//	write code here
		try {
			if(!assetId){
				showAlert("Es necesario el ID", "warning")
				return;
			}

			
			await temporal_process_asset_by_id.run({
				assetId,
				environment: env.selectedOptionValue
			})
			
			showAlert("Reprocesado iniciado con exito", "success")
			
			this.loadProcessingAssets()
		} catch(err){
			Sentry.captureException(err)
			if(err.message.includes("is already running")){
				showAlert("El reprocesado habia iniciado", "warning")
			}else {
				showAlert("Ocurrio un error", "error")
			}
		}

	},
	
	async loadProcessingAssets(){
		const listWorkflows = await list_workflows.run({
			environment: env.selectedOptionValue
		})
	
		
		this.workflows = listWorkflows.executions?.filter(workflow => workflow.type.name === "processAsset").map(workflow => ({
id: workflow.execution.workflowId,
			type: workflow.type.name,
			startTime: workflow.startTime,
			closeTime: workflow.closeTime,
			status: workflow.status,
			link: "https://temporal.production.internal.ticmas.io/namespaces/"+env.selectedOptionValue+"/workflows/"+workflow.execution.workflowId+"/"+workflow.execution.runId
})) ?? [];
		
		return this.workflows
	},
	
	async pollListWorkflows(){
		await this.loadProcessingAssets()
		
		setInterval(async () => {
			await this.loadProcessingAssets()
		}, 60000)
	}
}