export default {
	pollingIntervalId: null,
	hasUrl:create_url_google.text.trim() !== '',
	hasFile:create_upload_asset.files.length > 0,

	resetWidgets(){
		resetWidget(create_asset_title.text)
		resetWidget(create_subject_content_select.selectedOptionLabel)
		resetWidget("create_upload_asset")
		resetWidget("create_url_google")
	},
	disableReplaceAssetBtn(){
		return (!this.hasUrl && !this.hasFile)
	},

	async createAsset(){
		closeModal(create_asset_modal.name)
		if(this.hasUrl){
			await this.uploadAssetByUrl()
		}
		if(this.hasFile){
			await this.uploadAssetByFile()
		}
	},
	async uploadAssetByFile(){
		const assetId = (new bson.ObjectId()).toString()
		const assetToUpload = create_upload_asset.files?.find(file => file)
		const file_path = `resources/${assetId}/${assetToUpload?.name}`

		const newAsset = {
			_id: assetId,
			unprocessedUrl: file_path,
			title: create_asset_title.text,
			status: "in-progress",
			contentType: create_subject_content_select.selectedOptionValue
		}
		
		this.resetWidgets()
		console.log('subiendo asset')
		create_asset_button.setDisabled(true)
		await this.uploadAssets(newAsset,assetToUpload,file_path)


	},
	async uploadAssetByUrl(){
		try{
			storeValue('googleUrl', create_url_google.text, false)
			const response = await upload_from_googleDrive.run()
			console.log(response,'google response')
			if(response){
				resetWidget('url_input')

				showAlert('Asset agregado desde URL de google','success')

			}else{
				const errorMessage = response.data.error
				throw new Error(errorMessage)
			}
		}catch(err){
			console.error(err)
			showAlert(`${err.message}`, 'error')
		}
	},

	async uploadAssets(newAsset,assetToUpload,file_path){
		// console.log(newAsset,assetToUpload,)
		try {
			await upload_file.run({
				bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
				file_path,
				content: assetToUpload
			})

			if(upload_file.data){
				console.log("archivo subido")
				await create_asset.run({body:newAsset})

				if(create_asset.data){
					await showAlert(`Asset ${newAsset.title} creado con exito!`, "success").then(()=> create_asset_button.setDisabled(false))
					closeModal(create_asset_modal.name)
				}
			}
		} catch(err){
			console.error(err)
			create_asset_button.setDisabled(false)
			await showAlert(`Ocurrio un error creando el Asset: ${err}`, 'error')
		}
	},
}