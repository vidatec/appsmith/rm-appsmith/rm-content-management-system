export default {
	assets: [],
	assetLoading: true,
	selectedAsset: {},
	
	assetSchema: {
_id: new bson.ObjectId(),
			title: '',
			contentType: 'secuencia',
		unprocessedUrl: ''
	},
	
	setSelectedAsset(asset){
		console.log({asset})
		this.selectedAsset = asset
		showModal('asset_detail_modal')
	},
	
	async createSchema(){
		this.assetSchema = {
		_id: new bson.ObjectId(),
		title: '',
		contentType: 'secuencia',
		unprocessedUrl: ''
	}
},
	
	async getAssets(){
		await get_all_assets.run()
		this.assets = get_all_assets.data.map(asset => ({ 
			...asset, 
			unprocessedUrl: `https://cdn.${entornos.selectedOptionValue}.ticmas.io/${asset.unprocessedUrl}`, 
			url: `https://cdn.${entornos.selectedOptionValue}.ticmas.io/${asset.url}`, 
			zipUrl: `https://cdn.${entornos.selectedOptionValue}.ticmas.io/${asset.zipUrl}`}))
			this.assetLoading = false
		return this.assets
	},
	
	async loadData(){
		await Promise.all([
			this.getAssets(),
		])
	},
	
	async createAsset(){
		const assetId = (new bson.ObjectId()).toString()
		const assetToUpload = upload_file_subject.files?.find(file => file)
		const file_path = `resources/${assetId}/${assetToUpload?.name}`
		
		try {
			await upload_file.run({
				bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
				file_path,
				content: assetToUpload
			})
		
			console.log(upload_file.data)
			
			if(upload_file.data){
				console.log("archivo subido")
				await create_asset.run({
					body: {
						_id: assetId,
						unprocessedUrl: file_path,
						title: upload_title.text,
						status: "in-progress",
						contentType: subject_content_selector.selectedOptionValue
					}
				})

				if(create_asset.data){
					await showAlert("Asset creado con exito!", "success")
					await closeModal("crear_asset_modal")
				}
			}
			
		} catch(err){
			console.error(err)
			await showAlert("Ocurrio un error", 'error')
		}
	}
}