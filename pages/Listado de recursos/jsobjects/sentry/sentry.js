export default {

	async initSentry () {
		//	use async-await or promises
		//	await storeValue('varName', 'hello world')
		try {
			Sentry.init({
				dsn: "https://6c7f684787098d953b5a40012b3d7177@sentry.io/4507815618740224",
				// This sets the sample rate to be 10%. You may want this to be 100% while
				// in development and sample at a lower rate in production
				replaysSessionSampleRate: 0.01,

				// If the entire session is not sampled, use the below sample rate to sample
				// sessions when an error occurs.
				replaysOnErrorSampleRate: 1.0,
				integrations: function (integrations) {
					// integrations will be all default integrations
					// return integrations.filter(function (integration) {
					// console.log(integration)
					// return integration.name !== "Breadcrumbs";
					// });
					return [Sentry.replayIntegration({
      // Additional SDK configuration goes in here, for example:
      maskAllText: true,
      blockAllMedia: true,
    })]
				},
				// debug: true
			})
			
			Sentry.setUser({ 
				email: appsmith.user.email, 
				username: appsmith.user.username 
			})
		}catch(err){
			console.error(err)
		}
	}
}