export default {

	updatedSubjectName: '',
	subject: {},
	isLoadingList: 0,
	firstLoad: get_resources_appsmith.data,
	selectedOption: appsmith.URL.hostname.includes('develop') ? 'integration' : 'colegios',

	async searchResources(){
		const resources = await get_resources_appsmith.run({q: search.text})
		storeValue('resourcesDAta', resources.data, false)
		// if(search.text.length > 0){
		// await get_resources_list.run({q: search.text})
		// }
	},

	async dataToShow(){
		const {data} = await get_resources_appsmith.run()
		if(data)
			storeValue('resourcesDAta', data, false)
	}
}