export default {
	tagsToSelect: [],
	environmentToSelect: [],
	resources: [],
	library: {},
	syncStatus: "No iniciada ninguna sincronización",

	async getTags(){
		await listTags.run()
		const tags = listTags.data

		this.tagsToSelect = tags.map((tag) => ({"label": tag.name || tag.type, "value": tag._id}))
	},
	async getEnvironments(){
		await listEnvironments.run()

		const environments = listEnvironments.data

		this.environmentToSelect = environments.map(environment => ({"label": environment.name, "value": environment._id}))
	},

	async getDetailLibrary(){
		await detailLibrary.run()

		this.library = detailLibrary.data
	},

	async getResourcesByTags(){
		await listResourcesByTags.run({tags: this.library.tags.toString()})
		this.resources = listResourcesByTags.data
	},

	async updateLibrary(){
		await updateLibrary.run({
			payload: edit_form.formData
		})

		await this.initLoad()
	},

	async getWorkflowStatus(){
		await getTemporalWorkflow.run()
		
		const mappingStatus = {
			"default": ""
		}

		this.syncStatus = getTemporalWorkflow.data.workflowExecutionInfo.status ?? "No iniciada ninguna sincronización"
	},

	async syncLibrary(){
		await syncLibrary.run()
		await this.getWorkflowStatus()

    setTimeout(async ()=> {
			await this.getWorkflowStatus()
		}, 2000)
	},

	async initLoad(){
		await this.getDetailLibrary()
		await Promise.all([this.getEnvironments(), this.getTags()])
		await this.getResourcesByTags()
		await this.getWorkflowStatus()
	}
}