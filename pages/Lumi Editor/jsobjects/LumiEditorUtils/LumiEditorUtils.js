export default {	
		filePath: null,
	fileToUpload: null,
	contentLength: null,

	async Iframe1onMessageReceived(message){
		try {			
			return await new Promise((resolve, reject) => {
				console.log(message)
				const file = message.file
				const reader = new FileReader();
				reader.onloadend = () => {
					const contents = btoa(reader.result)
					this.fileToUpload = contents
					this.contentLength = contents.length
					this.filePath = 'h5p-lumi-assets/' + file.name.replace('.h5p', '.zip')				
					console.log(this)
					h5p_upload.run().then(() => resolve(true))
				}
				reader.onerror = e => reject(e)

				reader.readAsBinaryString(file)
			})
		} catch(err){
			console.log(err)
		} finally {
			const assetId = (new bson.ObjectId()).toString()
			const newAsset = {
				_id: assetId,
				unprocessedUrl: h5p_upload.data.url,
				title: message.file.name,
				status: "in-progress",
				contentType: "actividades generadoras"
			}
			
			await create_asset.run({body: newAsset})
			await h5p_upload.clear()
			this.filePath = null
			this.fileToUpload = null
			this.contentLength = null
		}
	}
}