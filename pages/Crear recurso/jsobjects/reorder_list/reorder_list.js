export default {

	async reorderRows(direction, rowIndex) {
		try {
			const assets = appsmith.store.assetList;

			if (direction === "up" && rowIndex > 0) {
				[assets[rowIndex - 1], assets[rowIndex]] = [assets[rowIndex], assets[rowIndex - 1]];
			} else if (direction === "down" && rowIndex < assets.length - 1) {
				[assets[rowIndex], assets[rowIndex + 1]] = [assets[rowIndex + 1], assets[rowIndex]];
			}

			storeValue("assetList", assets, false);
		} catch (error) {
			console.error("Error al reordenar las filas:", error);
			showAlert("Error al reordenar los assets.", "error");
		}
	},

	arrowVisible(rowIndex) {
		const assets = assets_list_table.tableData;
		const isFirstRow = rowIndex === 0;
		const isLastRow = rowIndex === assets.length - 1;
		return {
			showUpArrow: !isFirstRow,
			showDownArrow: !isLastRow
		};
	}

}