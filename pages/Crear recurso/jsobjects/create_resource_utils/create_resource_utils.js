export default {
	typeTagsToSearch: [
		'materia','nivel','contenido','reading_fluency_level',
		'pilar_pedagogico','temas_generales','situacion de ensenanza',
		'module','pais','modalidad','capacidades','eje','género','submodulo_robotica',
		'language','microcontenidos','submodulo_comprension_lectora',
		'alfabetizacion','actividad','ma_tipo_cursada','ma_espacio_curricular','ma_bimestre','ma_area','ma_codigo_area','ma_codigo_espacio','ma_nivel_idiomas'
	],
	tagSelectors: [
		module_tags,
		modality_tags,
		pedagogical_tag,
		submodule_tags,
		activity_tags,
		gender_tags,
		microContent_tags,
		robotic_tags,
		literacy_tags,
		readingFluency_tags,
		language_tags,
		ejeTematico_tags,
		generalTopics_tags,
		Tipo_de_cursada,
		ma_espacio_curricular,
		ma_bimestre,
		ma_area,
		ma_codigo_area,
		ma_codigo_espacio,
		ma_nivel_idiomas
	],
	tabsLevelSelector: [
		jurisdiction_selector,
		subject_selector,
		level_selector
	],
	jurisdictions: [],
	subjects:[],
	levels:[],
	tagsResults: {},
	pollingIntervalId: null,
	createResource: {},
	hasUrl: url_input.text.trim() !== '',
	hasFile: upload_file_asset.files.length > 0,
	hasTitle: create_resource_form.formData.title.trim() !== "",
	hasAssets: appsmith.store.assetList && appsmith.store.assetList.length > 0,
	assetsList: appsmith.store.assetList,
	environment: entornos.selectedOptionValue,
	selectedEnvironmentOption: appsmith.URL.hostname.includes('develop') ? 'integration' : 'colegios',

	allAssets:[],

	async loadAllAssets(){
		await get_assets_list.run()
		this.allAssets = await get_assets_list.data
		return this.allAssets
	},

	assetsListMemory(){
		const assetsListOnMemory = appsmith.store.assets_list_memory
		storeValue('assetList', assetsListOnMemory, false)
		closeModal(assets_list_modal.name)
		// assets_list_modal_table.setSelectedRowIndices([])
	},

	shouldEnablePublishButton() {
		return (this.hasTitle && this.hasAssets);
	},

	rowBeingEdited(){
		const rows = assets_list_table['editableCell'].index
		return rows !== -1
	},

	disableAssetToListBtn(){
		if(tabs.selectedTab === 'Subir'){
			return (!this.hasUrl && !this.hasFile)
		}
	},
	resetWidgets(){
		resetWidget("upload_file_asset")
		resetWidget("upload_title")
		resetWidget("subject_content_selector")
	},

	reset(){
		clearStore()
		resetWidget('create_resource_form')
		this.tabsLevelSelector.forEach(tagSelector =>{
			console.log(tagSelector.widgetName)
			resetWidget(`${tagSelector.widgetName}`)
		})
		this.tagSelectors.forEach(tagSelector =>{
			console.log(tagSelector.widgetName)
			resetWidget(`${tagSelector.widgetName}`)
		})

		navigateTo('Listado de recursos')
	},

	async initializeAssetTable(){
		const existingAssets = [];
		storeValue("assetList", existingAssets);
	},

	updateFrom(){
		const updatedFields = create_resource_form?.formData;
		for (const [key, value] of Object.entries(updatedFields)) {
			this.createResource[key] = value
		}
	},

	updateLevelForm(){
		const selectedJurisdictions = jurisdiction_selector.selectedOptionValues;
		const selectedSubjects = subject_selector.selectedOptionValues;
		const selectedLevels = level_selector.selectedOptionValues;

		this.createResource = {
			...this.createResource,
			jurisdictions: selectedJurisdictions,
			subjects: selectedSubjects,
			levels: selectedLevels
		}
	},

	updateAttributeTagsForm(){
		const allTags = []
		this.tagSelectors.forEach(selector =>{
			if(selector.selectedOptionValues){
				allTags.push(...selector.selectedOptionValues.map(tag=>({ _id: tag })))
			}
		})

		this.createResource = {
			...this.createResource,
			tags: allTags
		}

	},

	async loadTags() {
		const results = await Promise.all(
			this.typeTagsToSearch.map(type => tags.run({ type })
																.then(result => ({ 
				type, 
				data: result.map(tag => ({ label: tag.name, value: tag._id })) 
			}))
															 ));

		this.tagsResults = results.reduce((acc, { type, data }) => {
			acc[type] = data;
			return acc;
		}, {});

		storeValue('tagsResults', this.tagsResults);
	},

	async getSelectorsData(environment){
		let jurisdictionsData, levelsData, subjectsData, jurisdictionGroup;

		switch (environment) {
			case 'beta':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_beta.run(),
					get_levels_beta.run(),
					get_subjects_beta.run(),
					get_juris_groups_beta.run()
				]);

			case 'colegios':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_colegios.run(),
					get_levels_colegios.run(),
					get_subjects_colegios.run(),
					get_juris_groups_colegios.run()
				]);

			case 'profesional':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_profesional.run(),
					get_levels_profesional.run(),
					get_subjects_profesional.run(),
					get_juris_groups_profesional.run()
				]);

			case 'integration':
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_integration.run(),
					get_levels_integration.run(),
					get_subjects_integration.run(),
					get_juris_groups_integration.run()
				]);

			default:
				return [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await Promise.all([
					get_jurisdictions_colegios.run(),
					get_levels_colegios.run(),
					get_subjects_colegios.run(),
					get_juris_groups_colegios.run()
				]);
		}
	},

	async getSelectors(){
		const [jurisdictionsData, levelsData, subjectsData, jurisdictionGroup] = await this.getSelectorsData(this.environment);
		return{
			jurisdictions: jurisdictionsData?.map(j =>({
				label: j.name,
				value: j.id
			})),
			levels: levelsData?.map(lv => ({
				label: lv.name,
				value: lv.id
			})),
			subjects: subjectsData?.map(s => ({
				label: s.name,
				value: s.id
			})),
			jurisdictionGroup: jurisdictionGroup
		}	
	},

	//pdf file validation
	pdfValidation() {
		const guideToUpload = create_teacher_guied.files?.[0];
		const allowedTypes = ["application/pdf"];
		if (!guideToUpload || !allowedTypes.includes(guideToUpload.type)) {
			showAlert('Debes seleccionar un archivo de tipo PDF', "error");
			resetWidget('create_teacher_guied');
			return false;
		}

		return true;
	},
	async uploadGuiaDocente(resourceId){

		try{
			if(create_teacher_guied.files.length >= 1 && this.pdfValidation()){
				const guideToUpload = create_teacher_guied.files?.find(file => file)
				const file_path = `resoruces/${resourceId}/${guideToUpload?.name}`

				const data = await upload_guia_docente.run({
					bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
					file_path,
					content: guideToUpload
				})

				if(data){
					showAlert('Guia guardada correctamente!', 'success', 4000)
					this.createResource = {
						...this.createResource,
						guideUrl: file_path
					}
					storeValue('createResource', this.createResource, false)
					resetWidget('create_teacher_guied')
				}	
			} else{
				delete this.createResource.guideUrl
				storeValue('createResource', this.createResource)
			}
		} catch(err){
			console.log(err)
			showAlert(`Error al guarar el cover de la imagen! ${err}`, 'error', 4000)
		}
	},
	async uploadCoverImage(resourceId){
		try {
			if(create_cover_image.files.length >= 1){
				const coverToUpload = create_cover_image.files?.find(file => file)
				const file_path = `resources/${resourceId}/${coverToUpload?.name}`

				console.log('se activa el uploadCoverImage')
				const data = await upload_cover_image.run({
					bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
					file_path,
					content: coverToUpload
				})
				if(data){
					showAlert('Cover guardado correctamente!', 'success', 4000)
					this.createResource = {
						...this.createResource,
						coverUrl: file_path
					}
					await storeValue('createResource', this.createResource, false)
					resetWidget('create_cover_image')
				}
			} else {
				delete this.createResource.coverUrl
				await storeValue('createResource', this.createResource)
			}
		} catch(err){
			console.error(err)
			showAlert(`Error al guarar el cover de la imagen! ${err}`, 'error', 4000)
		}
	},

	async uploadAssetByUrl(){
		try{
			const response = await upload_from_googleDrive.run()
			console.log(response,'google response')
			if(response){
				resetWidget('url_input')

				const newAsset ={
					_id: response._id,
					title: response.title,
					status: response.status
				}
				const currentAssetList = appsmith.store.assetList || []
				const updatedAssetList = [...currentAssetList, newAsset]
				console.log(updatedAssetList, 'DEBERIA TENER EL ASSET ID AGREGADO DE LA URL DE GOOGLE')
				showAlert('Asset agregado desde URL de google','success')
				storeValue("assetList", updatedAssetList, false);

			}else{
				const errorMessage = response.data.error
				throw new Error(errorMessage)
			}
		}catch(err){
			console.error(err)
			showAlert(`${err.message}`, 'error')
		}
	},
	async uploadAssetByFile(){
		const assetId = (new bson.ObjectId()).toString()
		const assetVersionId = (new bson.ObjectId()).toString()
		const assetToUpload = upload_file_asset.files?.find(file => file)
		const file_path = `assets/${assetId}/versions/${assetVersionId}/${assetToUpload?.name}`//cambiar para usar con el versionado de assets

		const newAsset = {
			_id: assetId,
			assetVersionId,
			unprocessedUrl: file_path,
			title: upload_title.text,
			status: "in-progress",
			contentType: subject_content_selector.selectedOptionValue
		}
		const currentAssetList = appsmith.store.assetList || []
		const updatedAssetList = [...currentAssetList, newAsset]
		console.log(updatedAssetList, 'DEBERIA TENER EL ASSET AGREGADO')
		this.resetWidgets()

		if(tabs.selectedTab === 'Subir'){
			console.log('se activa el subir')
			await this.uploadAssets(newAsset,assetToUpload,file_path)
			storeValue("assetList", updatedAssetList, false);
		}

	},

	async addAssetsToList(){
		publish_content.setDisabled(true)

		if(this.hasUrl){
			await this.uploadAssetByUrl()
			publish_content.setDisabled(false)
		}
		if(this.hasFile){
			await this.uploadAssetByFile()
			publish_content.setDisabled(false)
		}

	},

	async uploadAssets(newAsset,assetToUpload,file_path){
		console.log('uploadAssets',newAsset)
		try {
			await upload_asset_file.run({
				bucket_name: `cdn.${entornos.selectedOptionValue}.ticmas.io`,
				file_path,
				content: assetToUpload
			})
console.log(upload_asset_file.data)
			
			if(upload_asset_file.data){
				console.log("archivo subido")
				
				await create_asset.run({body:newAsset})
				if(create_asset.data){
					await showAlert(`Asset ${newAsset.title} creado con exito!`, "success").then(()=> publish_content.setDisabled(false))
				}
			}
		} catch(err){
			console.error(err)
			publish_content.setDisabled(false)
			await showAlert(`Ocurrio un error procesando el asset: ${err.message}`, 'error')
		}
	},

	async pollingAssets(){
		try{
			const {assets} = await resource_detail.run()
			// storeValue("assetList", assets, false)
			const allProcessed = assets.every(asset => asset.status !== "in-progress")

			if (allProcessed) {
				clearInterval(this.pollingIntervalId);
				console.log("Polling detenido: todos los assets procesados")
				storeValue("assetList", assets);
				publish_content.setDisabled(false)
			}else{
				clearInterval(this.pollingIntervalId)
				console.log("Polling detenido: todos los assets procesados")
				storeValue("assetList", assets)
				publish_content.setDisabled(false)
			}

		}catch(err){
			console.error(err)
			showAlert(`Ocurrio un error: ${err}`, 'error')
		}
	},

	async pollingInit(){
		publish_content.setDisabled(true)
		// init the polling if there a "in-progress" asset
		if (appsmith.store.assetList.some(asset => asset.status === "in-progress")) {
			this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
			console.log("Polling iniciado algun asset con in-progress");
		}else{
			console.log("Polling iniciado por url de google");
			this.pollingIntervalId = setInterval(() => this.pollingAssets(), 5000);
		}
	},

	async createNewResource(){
		publish_content.setDisabled(true)
		try{

			const newResource = {
				...this.createResource,
				assets: this.assetsList.map(({_id}) => _id)
			};

			await storeValue('createResource', newResource)
			console.log(newResource, 'antes de guardar todo el recurso')

			const response = await create_resource.run()
			console.log(response ,'despues de haber creado el recurso')

			// update  del recurso para agregarle cover y la guia
			if(response._id){

				await Promise.all([
					this.uploadCoverImage(response._id),
					this.uploadGuiaDocente(response._id)
				]);

				// for (const key in newResource) {
				// if (Array.isArray(newResource[key]) && newResource[key].length === 0) {
				// delete newResource[key];
				// }
				// }

				const r = await update_resource_created.run({id:response._id})
				console.log(r,'recurso actualizado?')
			}

			await publish_resource.run({_id:response._id}).then((showAlert('Contenido publicado', 'success')))
			publish_content.setDisabled(false)
			clearStore()
			showModal(success_modal.name)
		}catch(err){
			console.error(err)
			showAlert(`Ocurrio un error al guardar: ${err}`, 'error')
			publish_content.setDisabled(false)
		}
	},

	async updateAssetOnListTable(){
		const {_id, title} = await get_asset_api.run()
		const updatedAssetList = appsmith.store.assetList.map(asset => {
			if (asset._id === _id) {
				return { ...asset, title }; 
			} else {
				return asset;
			}
		});
		storeValue("assetList", updatedAssetList);
	},

	async updateAssetTilte(){
		const newTitle = assets_list_table.updatedRow.title
		const currentRowId = assets_list_table.updatedRow._id
		const currentTitle = appsmith.store.assetList.find(asset => asset._id === currentRowId)?.title
		storeValue('assetTitle', newTitle)

		if(newTitle !== currentTitle){
			publish_content.setDisabled(true)
			await update_asset.run()
				.then(()=> this.updateAssetOnListTable())
				.then(()=> showAlert(`Asset actulizado`, 'success'))
			publish_content.setDisabled(false)
		}
	}

}